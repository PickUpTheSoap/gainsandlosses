/// Get all the gains and losses from the prices list
/// Assume prices are always ordered and the precision of price is always 2 decimal places
///
/// - Parameter prices: An ordered list of prices
/// - Returns: return a tuple including the gains and losses
func getGainsAndLossesDouble(prices: [Double]) -> (gains: Double, losses: Double) {
    var gains: Double = 0, losses: Double = 0
    for index in 0..<prices.count {
        guard index > 0 else { continue }
        let currentPrice = prices[index] * 100
        let previousPrice = prices[index-1] * 100
        if currentPrice >= previousPrice {
            gains += currentPrice - previousPrice
        } else {
            losses -= previousPrice - currentPrice
        }
    }
    gains /= 100
    losses /= 100
    print(gains, losses)
    return (gains, losses)
}

/*
 Here I just want to explain why I choose the solution above with Double instead of Decimal with Swift.
 
 In order to use Decimal, you have to get a list of Double or String, otherwise if you are using Decimal,
 you cannot get the correct answer with some specific input like 78.41. After trying, when you assign Decimal(78.41), this
 variable is 78.40999999999997952 rather than 78.41, because 78.41 itself cannot present value "78.41" exactly. Although we can use
 Decimal(sign: .plus, exponent: -2, significand: 7841) to initialise instead, we cannot change all input to Int.
 
 If using [Double] as input, you have to transform Double to String and initialise the Decimal.
 If using [String] as input, there is no need for transforming, but I don't think it is a good idea to store numbers as String.
 
 I will attach my Decimal solution as below, but I prefer the first solution with Double input and Double output.
 Thanks
*/

/// Get all the gains and losses from the prices list
/// Assume prices are stored in order and the input is Double.
///
/// - Parameter prices: An ordered list of prices
/// - Returns: return a tuple including the gains and losses
func getGainsAndLossesDecimal(prices: [Double]) -> (gains: Decimal, losses: Decimal) {
    var gains: Decimal = 0
    var losses: Decimal = 0
    for index in 0..<prices.count {
        guard index > 0 else { continue }
        guard let currentPrice = Decimal(string: prices[index].description),
            let previousPrice = Decimal(string: prices[index-1].description) else { fatalError("Incorrect Number") }
        if currentPrice >= previousPrice {
            gains += currentPrice - previousPrice
        } else {
            losses -= previousPrice - currentPrice
        }
    }
    print(gains, losses)
    return (gains, losses)
}
